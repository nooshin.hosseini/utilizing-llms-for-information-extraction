import openai

api_key = "sk-proj-hnn1crEpJr2tOpSZHS1bT3BlbkFJT6VNkAqd0oTvveRVCNGW"


openai.api_key = api_key

# Function to identify the introduction
def identify_introduction(text):
    prompt = (
        "Here is an article:\n\n"
        f"{text}\n\n"
        "Please identify the 'introduction' section of the article."
    )
    response = openai.Completion.create(
        engine="text-davinci-003",  
        prompt=prompt,
        max_tokens=200  
    )
    return response.choices[0].text.strip()

# Function to list place names
def list_place_names(text):
    prompt = (
        "Here is an article:\n\n"
        f"{text}\n\n"
        "Please list all the place names mentioned in the text."
    )
    response = openai.Completion.create(
        engine="text-davinci-003",  
        prompt=prompt,
        max_tokens=200  
    )
    return response.choices[0].text.strip()

# Example article text
with open('text3.txt', 'r') as file:
    article_text = file.read('text3.txt')


# Identify the introduction
introduction = identify_introduction(article_text)
print("Introduction Section:")
print(introduction)

# List place names
place_names = list_place_names(article_text)
print("\nPlace Names:")
print(place_names)

#another question
#second change
